﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrainingRooms;

namespace Trainingrooms.Droid
{
    [Activity(Label = "TrainingRoomDetailActivity")]
    public class TrainingRoomDetailActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.TrainingRoomDetail);

            int roomId = Intent.GetIntExtra("roomId", 0);
            RoomRepository repo = new RoomRepository();
            TrainingRoom room = repo.GetRoom(roomId);

            this.Title = "Room Detail";
            this.FindViewById<TextView>(Resource.Id.txtName).Text = "Name: " + room.Name;
            this.FindViewById<TextView>(Resource.Id.txtLocation).Text = "Building: " + room.Building;
        }
    }
}