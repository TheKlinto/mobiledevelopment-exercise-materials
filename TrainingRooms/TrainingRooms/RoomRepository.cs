﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace TrainingRooms
{
    public class RoomRepository
    {
        private List<TrainingRoom> rooms =
            new List<TrainingRoom>
            {
                new TrainingRoom
                {
                    Id = 1,
                    Name = "Science",
                    Building = "B1",
                    ComputerNumbers = 5
                },
                new TrainingRoom
                {
                    Id = 2,
                    Name = "Chemistry",
                    Building = "B1",
                    ComputerNumbers = 17
                },
                new TrainingRoom
                {
                    Id = 3,
                    Name = "Physics",
                    Building = "B3",
                    ComputerNumbers = 1
                }
            };
        
        public RoomRepository()
        {
        }

        public List<TrainingRoom> GetRooms()
        {
            return rooms;
        }
        public TrainingRoom GetRoom(int id)
        {
            return rooms.FirstOrDefault(room => room.Id == id);
        }
    }
}
