﻿using System;

namespace TrainingRooms
{
    public class TrainingRoom
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Building { get; set; }
        public int ComputerNumbers { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
