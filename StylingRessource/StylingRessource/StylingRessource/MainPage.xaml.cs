﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace StylingRessource
{
    public partial class MainPage : ContentPage, INotifyPropertyChanged
    {
        public MainPage()
        {
            InitializeComponent();
            BindingContext = this;

            Random = new Random();
            Number = 0;

        }

        int _number;
        public int Number
        {
            get => _number;
            set
             {
                _number = value;
                OnPropertyChanged(nameof(Number));
            }
        }
        
        public Random Random { get; set; }

        private void Button_Clicked(object sender, EventArgs e)
        {
            App.Current.Resources["NumberColor"] = Color.FromRgb(
                Random.Next(256),
                Random.Next(256),
                Random.Next(256));
            Number = _number + 1;
        }
    }
}
