﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinShell
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new AppShell();
            Routing.RegisterRoute(nameof(Views.ElephantView), typeof(Views.ElephantView));
            Routing.RegisterRoute(nameof(Views.SnakeView), typeof(Views.SnakeView));
            Routing.RegisterRoute(nameof(Views.HippoView), typeof(Views.HippoView));
            Routing.RegisterRoute(nameof(Views.TurtleView), typeof(Views.TurtleView));
            Routing.RegisterRoute(nameof(Views.ParrotView), typeof(Views.ParrotView));
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
