﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace VitaSimPromotion
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new AppShell();

            Routing.RegisterRoute(nameof(Views.MainPage), typeof(Views.MainPage));
            Routing.RegisterRoute(nameof(Views.ProjectPageView), typeof(Views.ProjectPageView));
            Routing.RegisterRoute(nameof(Views.VRRoomControlView), typeof(Views.VRRoomControlView));
            Routing.RegisterRoute(nameof(Views.VRRoomListView), typeof(Views.VRRoomListView));
            Routing.RegisterRoute(nameof(Views.ARCameraView), typeof(Views.ARCameraView));
            Routing.RegisterRoute(nameof(Views.SettingsView), typeof(Views.SettingsView));
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
