﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VitaSimPromotion.Models;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace VitaSimPromotion.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VRRoomListView : ContentPage
    {
        List<VRRoom> vrRooms;

        public VRRoomListView()
        {
            InitializeComponent();
            SeedRooms(6);
            VRRoomCollection.ItemsSource = vrRooms;
        }

        public void SeedRooms(int amount)
        {
            Random rnd = new Random();
            for(int i = 0; i < amount; i++)
            {
                string id = "";
                for(int j = 0; j < 6; j++)
                {
                    id += rnd.Next(0, 9).ToString();
                }

                vrRooms.Add(new VRRoom
                {
                    Id = Int32.Parse(id)
                });
            }
        }
    }
}