﻿using Android.Content.PM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace VitaSimPromotion.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ARCameraView : ContentPage
    {
        public ARCameraView()
        {
            InitializeComponent();
        }

        private async void ARCamera_Button(object sender, EventArgs e)
        {
            try
            {
                await Launcher.OpenAsync(new OpenFileRequest
                {
                    File = new ReadOnlyFile("dk.Klinthansen.FoodPlacementAR")
                });
            }
            catch(Exception)
            {
                await DisplayAlert("Fejl", "AR Appen er ikke installeret", "OK");
            }
        }

    }
}