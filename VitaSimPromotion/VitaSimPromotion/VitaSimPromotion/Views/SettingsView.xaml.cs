﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace VitaSimPromotion.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingsView : ContentPage
    {
        public SettingsView()
        {
            InitializeComponent();
        }

        private void OrangeTheme_Clicked(object sender, EventArgs e)
        {
            App.Current.Resources["MainColor"] = App.Current.Resources["VitaSimOrange"];
        }

        private void BlueTheme_Clicked(object sender, EventArgs e)
        {
            App.Current.Resources["MainColor"] = App.Current.Resources["VitaSimBlue"];
        }
    }
}