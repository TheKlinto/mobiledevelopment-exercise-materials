﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace VitaSimPromotion.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void TryARButton_Clicked(object sender, EventArgs e)
        {
            Shell.Current.GoToAsync(nameof(ARCameraView));
        }

        private void AboutProjectButton_Clicked(object sender, EventArgs e)
        {
            Shell.Current.GoToAsync(nameof(ProjectPageView));
        }

        private void VRRoomButton_Clicked(object sender, EventArgs e)
        {
            Shell.Current.GoToAsync(nameof(VRRoomListView));
        }

        private void SettingsButton_Clicked(object sender, EventArgs e)
        {
            Shell.Current.GoToAsync(nameof(SettingsView));
        }
    }
}