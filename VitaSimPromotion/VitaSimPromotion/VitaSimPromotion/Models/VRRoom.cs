﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VitaSimPromotion.Models
{
    class VRRoom
    {
        public int Id { get; set; }
        public int Time { get; set; }
        public string Color { get; set; }
    }
}
