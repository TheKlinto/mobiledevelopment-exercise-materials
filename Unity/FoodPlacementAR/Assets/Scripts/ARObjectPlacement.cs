﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.UI;


//Movement script (seperat uden gameobject, og pass i parameter)
public class ARObjectPlacement : MonoBehaviour
{

    public ARSessionOrigin aRSessionOrigin;

    public List<ARRaycastHit> raycastHits = new List<ARRaycastHit>();
    public GameObject drill;
    private GameObject instantiatedDrill;
    public GameObject HidePlanesButton, ShowPlanesButton, SpawnDrillButton, DespawnDrillButton, RightArrowButton, LeftArrowButton;

    List<GameObject> ObjectControl;

    // Start is called before the first frame update
    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        aRSessionOrigin.GetComponent<ARPlaneManager>().enabled = true;

        
        ObjectControl = new List<GameObject>(){ DespawnDrillButton, RightArrowButton, LeftArrowButton};
        //Disabling UI at start
        ShowPlanesButton.gameObject.SetActive(false);
        foreach(var button in ObjectControl)
        {
            button.gameObject.SetActive(false);
        } 

    } 

    // Update is called once per frame
    void Update()
    {
        /*
        // 1) Detect the user touch i.e when the user taps on the screen
        // 2) Project a raycast.
        // 3) Instantiate a virtual cube at the point where caycast meets the detected plane

        if(Input.GetMouseButton(0)) //Mousebutton(0) is left mousebutton in the case of pc, and counts as a touch on mobile devices
        {
            //Checks if the raycast is hitting a tracked plane, and returns a bool depending on the result
            bool collision = aRSessionOrigin.GetComponent<ARRaycastManager>().Raycast(Input.mousePosition, raycastHits, TrackableType.PlaneWithinPolygon);
            if(collision)
            {
                if(instantiatedDrill == null)
                {
                    instantiatedDrill = Instantiate(drill);

                    foreach(var plane in aRSessionOrigin.GetComponent<ARPlaneManager>().trackables)
                    {
                        plane.gameObject.SetActive(false);
                    }
                    planesActive = false;

                    aRSessionOrigin.GetComponent<ARPlaneManager>().enabled = false;
                }

                instantiatedDrill.transform.position = raycastHits[0].pose.position;
            }
        }
        */
    }

    public void PlaceObject()
    {
        bool collision = aRSessionOrigin.GetComponent<ARRaycastManager>().Raycast(Input.mousePosition, raycastHits, TrackableType.PlaneWithinPolygon);
            if(collision)
            {
                if(instantiatedDrill == null)
                {
                    instantiatedDrill = Instantiate(drill);
                    instantiatedDrill.transform.position = raycastHits[0].pose.position;

                    if(aRSessionOrigin.GetComponent<ARPlaneManager>().enabled)
                    {
                        ManipulatePlanes();
                    }
                }

                SpawnDrillButton.gameObject.SetActive(false);
                foreach(var button in ObjectControl)
                {
                    button.gameObject.SetActive(true);
                }
            }
    }
    
    public void RemoveObject()
    {
        if(instantiatedDrill != null)
        {
            Destroy(instantiatedDrill);
        }

        SpawnDrillButton.gameObject.SetActive(true);
        if(!aRSessionOrigin.GetComponent<ARPlaneManager>().enabled)
        {
            ManipulatePlanes();
        }
        aRSessionOrigin.GetComponent<ARPlaneManager>().enabled = false;
        foreach(var button in ObjectControl)
        {
            button.gameObject.SetActive(false);
        }
    }

    public void ManipulatePlanes()
    {
        if(aRSessionOrigin.GetComponent<ARPlaneManager>().enabled)
        {
            foreach(var plane in aRSessionOrigin.GetComponent<ARPlaneManager>().trackables)
            {
                plane.gameObject.SetActive(false);
            }
            foreach(var point in aRSessionOrigin.GetComponent<ARPointCloudManager>().trackables)
            {
                point.gameObject.SetActive(false);
            }
            aRSessionOrigin.GetComponent<ARPlaneManager>().enabled = false;
            aRSessionOrigin.GetComponent<ARPointCloudManager>().enabled = false;
            HidePlanesButton.gameObject.SetActive(false);
            ShowPlanesButton.gameObject.SetActive(true);
        }
        else
        {
            foreach(var plane in aRSessionOrigin.GetComponent<ARPlaneManager>().trackables)
            {
                plane.gameObject.SetActive(true);
            }
            foreach(var point in aRSessionOrigin.GetComponent<ARPointCloudManager>().trackables)
            {
                point.gameObject.SetActive(true);
            }
            aRSessionOrigin.GetComponent<ARPlaneManager>().enabled = true;
            aRSessionOrigin.GetComponent<ARPointCloudManager>().enabled = true;
            ShowPlanesButton.gameObject.SetActive(false);
            HidePlanesButton.gameObject.SetActive(true);
        }        
    }
}
