using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RotationScript : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    GameObject rotatedObject;
    [Header("Rotation settings")]
    [Tooltip("The speed at which the object rotates.\nStandard = 20")]
    public float RotationSpeed = 20;
    [Tooltip("What way to rotate object.\n1 = Right.\n-1 = Left")]
    public float RotationDirection = 0;
    bool rotate = false;

    // FixedUpdate calls once every 0.02 seconds.

    void FixedUpdate()
    {
        if (rotate)
        {
            if(RotationDirection == 1)
            {
                //rotatedObject.transform.parent.Rotate(Vector3.up * RotationSpeed * Time.deltaTime);
                rotatedObject.transform.RotateAround(rotatedObject.GetComponent<Renderer>().bounds.center, Vector3.up, RotationSpeed * Time.deltaTime);
            }
            else if(RotationDirection == -1)
            {
                //rotatedObject.transform.parent.Rotate(Vector3.down * RotationSpeed * Time.deltaTime);
                rotatedObject.transform.RotateAround(rotatedObject.GetComponent<Renderer>().bounds.center, Vector3.down, RotationSpeed * Time.deltaTime);
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        rotatedObject = GameObject.FindGameObjectWithTag("Drill");

        rotate = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        rotate = false;
    }
}
