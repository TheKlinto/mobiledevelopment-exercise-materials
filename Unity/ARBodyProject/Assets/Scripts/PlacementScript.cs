﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class PlacementScript : MonoBehaviour
{
    public GameObject ObjectToSpawn;
    public GameObject PlacementIndicator;
    public ARSessionOrigin SessionOrigin;
    private GameObject spawnedObject;
    private Pose PlacementPose;
    private ARRaycastManager raycastManager;
    private ARPointCloudManager cloudManager;
    private ARPlaneManager planeManager;
    private bool placementPoseIsValid = false;

    void Start()
    {
        //Loading all the managers from SessionOrigin.
        raycastManager = SessionOrigin.GetComponent<ARRaycastManager>();
        cloudManager = SessionOrigin.GetComponent<ARPointCloudManager>();
        planeManager = SessionOrigin.GetComponent<ARPlaneManager>();
    }

    // need to update placement indicator, placement pose and spawn 
    void Update()
    {
        if(placementPoseIsValid && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            if(spawnedObject == null)
            {
                ARPlaceObject();
            }
            else if(spawnedObject != null)
            {
                Destroy(spawnedObject);
            }


            
        }
        
        UpdatePlacementPose();
        UpdatePlacementIndicator();

    }
    void UpdatePlacementIndicator()
    {
        if(spawnedObject == null && placementPoseIsValid)
        {
            PlacementIndicator.SetActive(true);
            PlacementIndicator.transform.SetPositionAndRotation(PlacementPose.position, PlacementPose.rotation);
        }
        else
        {
            PlacementIndicator.SetActive(false);
        }
    }

    void UpdatePlacementPose()
    {
        var screenCenter = Camera.current.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
        var hits = new List<ARRaycastHit>();
        raycastManager.Raycast(screenCenter, hits, TrackableType.Planes);

        placementPoseIsValid = hits.Count > 0;
        if(placementPoseIsValid)
        {
            PlacementPose = hits[0].pose;
        }
    }

    void ARPlaceObject()
    {
        spawnedObject = Instantiate(ObjectToSpawn, PlacementPose.position, PlacementPose.rotation);
    }

    void PlaneManipulator()
    {
        if(spawnedObject)
        {
            cloudManager.enabled = false;
            foreach(var point in cloudManager.trackables)
            {
                point.gameObject.SetActive(false);
            }
            planeManager.enabled = false;
            foreach(var plane in planeManager.trackables)
            {
                plane.gameObject.SetActive(false);
            }  
        }
        else
        {
            cloudManager.enabled = true;
            foreach(var point in cloudManager.trackables)
            {
                point.gameObject.SetActive(true);
            }
            planeManager.enabled = true;
            foreach(var plane in planeManager.trackables)
            {
                plane.gameObject.SetActive(true);
            }
        }
        
    }

}
