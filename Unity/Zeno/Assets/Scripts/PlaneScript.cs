﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class PlaneScript : MonoBehaviour
{
    public ARSessionOrigin ARSessionOrigin;

    // Start is called before the first frame update
    void Start()
    {
        ARSessionOrigin.GetComponent<ARPlaneManager>().enabled = true;
        ARSessionOrigin.GetComponent<ARPointCloudManager>().enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
