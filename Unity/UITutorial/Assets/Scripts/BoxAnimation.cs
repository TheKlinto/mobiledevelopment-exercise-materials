﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class BoxAnimation : MonoBehaviour
{
    public GameObject Box_move;
    public GameObject Box_rotate;
    public GameObject Box_scale;

    public void MoveAnimation()
    {
        Box_move.GetComponent<Animation>().Play();
    }

    public void RotateAnimation()
    {
        Box_rotate.GetComponent<Animation>().Play();
    }

    public void ScaleAnimation()
    {
        Box_scale.GetComponent<Animation>().Play();
    }
}
