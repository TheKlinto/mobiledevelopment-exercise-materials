﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ImageRecognitionApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new CaemraPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
