﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using
using System.Net.Http;

namespace ImageRecognitionApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CaemraPage : ContentPage
    {
        public CaemraPage()
        {
            InitializeComponent();

            CameraButton.Clicked += CameraButton_Clicked;
        }

        private async void CameraButton_Clicked(object sender, EventArgs e)
        {
            var photo = await Plugin.Media.CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions() { });
            MemoryStream ms = new MemoryStream();
            byte[] imageStream;

            if(photo != null)
            {
                PhotoImage.Source = ImageSource.FromStream(() => { return photo.GetStream(); });
                photo.GetStream().CopyTo(ms);
                imageStream = ms.ToArray();
                
            }

            HttpClient client = new HttpClient();
            client.PostAsync("http://xamarin.surfspots.dk/");
        }


        
    }
}